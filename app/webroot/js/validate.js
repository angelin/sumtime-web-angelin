function validateUser(){
	
	var success = true;
	$('#UserName').css('border-color', '#d5d5d5'); 
	$('#UserUsername').css('border-color', '#d5d5d5');  
	
	if ($.trim($('#UserName').val()) == ''){
		$('#UserName').css('border', '1px solid red');
		success = false;
	}
	
	var extension = document.getElementById('UserUsername').value;
    extension = extension.split('@').pop();
    extension = extension.toLowerCase();
	if((extension!='inf.ufpel.edu.br') || !$.trim($('#UserUsername').val()).match("^([0-9,a-z,A-Z]+)([.,_]([0-9,a-z,A-Z]+))*[@]([inf]+)[.]([ufpel]+)[.]([edu]+)[.]([br]){1}([0-9,a-z,A-Z])?$")){
		$('#UserUsername').css('border', '1px solid red');
		success = false;
	}
	
	return success;
}

function validateUserEdit(){
	var success = true;
	
	$('#UserPasswordOld').css('border-color', '#d5d5d5'); 
	$('#UserPasswordNew').css('border-color', '#d5d5d5'); 
	$('#UserPasswordRepeat').css('border-color', '#d5d5d5');  
	
	if ($.trim($('#UserPasswordNew').val()) != $.trim($('#UserPasswordRepeat').val())){
		$('#UserPasswordRepeat').css('border', '1px solid red');
		if($.trim($('#UserPasswordOld').val()) == ''){
			$('#UserPasswordOld').css('border', '1px solid red');
		}
		success = false;
	}
	
	return success;
}

function validateUserConclusion(){
	
	var success = true;
	$('#UserCourseId').css('border-color', '#d5d5d5');
	$('#UserName').css('border-color', '#d5d5d5');
	$('#UserUsername').css('border-color', '#d5d5d5'); 
	$('#UserPassword').css('border-color', '#d5d5d5'); 
	
	if ($.trim($('#UserCourseId').val()) == ''){
		$('#UserCourseId').css('border', '1px solid red');
		success = false;
	}
	
	if ($.trim($('#UserName').val()) == ''){
		$('#UserName').css('border', '1px solid red');
		success = false;
	}
	
	if ($.trim($('#UserUsername').val()) == '' || !$.trim($('#UserUsername').val()).match("^([0-9,a-z,A-Z]+)([.,_]([0-9,a-z,A-Z]+))*[@]([inf]+)[.]([ufpel]+)[.]([edu]+)[.]([br]){1}([0-9,a-z,A-Z])?$")) {
		$('#UserUsername').css('border', '1px solid red');
		success = false;
	}
	
	if ($.trim($('#UserPassword').val()) == ''){
		$('#UserPassword').css('border', '1px solid red');
		success = false;
	}
	
	return success;
}

function validateAnalysiNew(){
	var success = true;
	
	$('#AnalysiFileNew').css('border-color', '#d5d5d5');
	
	var extension = document.getElementById('AnalysiFileNew').value;
    extension = extension.split('.').pop();
    extension = extension.toLowerCase();
	if((document.getElementById('AnalysiFileNew').value == '') || (extension!='xml')){
		$('#AnalysiFileNew').css('border', '1px solid red');
		success = false;
	}
	
	return success;
}

function validateActivity(){
	var success = true;
	
	$('#ActivityDocumentNew').css('border-color', '#d5d5d5');
	$('#ActivityName').css('border-color', '#d5d5d5');
	$('#ActivityHours').css('border-color', '#d5d5d5');
	$('#ActivityModalityId').css('border-color', '#d5d5d5'); 
	$('#ActivityTypeId').css('border-color', '#d5d5d5'); 
	
	var extension = document.getElementById('ActivityDocumentNew').value;
    extension = extension.split('.').pop();
    extension = extension.toLowerCase();
	if((document.getElementById('ActivityDocumentNew').value != '') && (extension!='pdf')){
		$('#ActivityDocumentNew').css('border', '1px solid red');
		success = false;
	}
	
	if ($.trim($('#ActivityName').val()) == ''){
		$('#ActivityName').css('border', '1px solid red');
		success = false;
	}
	
	if ($.trim($('#ActivityHours').val()) == ''){
		$('#ActivityHours').css('border', '1px solid red');
		success = false;
	}
	if ($.trim($('#ActivityModalityId').val()) == ''){
		$('#ActivityModalityId').css('border', '1px solid red');
		success = false;
	}
	if ($.trim($('#ActivityTypeId').val()) == ''){
		$('#ActivityTypeId').css('border', '1px solid red');
		success = false;
	}
	
	return success;
}

function validateCertificate(){
	var success = true;
	
	$('#ActivityDocument').css('border-color', '#d5d5d5');
	var extension = document.getElementById('ActivityDocument').value;
    extension = extension.split('.').pop();
    extension = extension.toLowerCase();
	if((document.getElementById('ActivityDocument').value == '') || (extension!='pdf')){
		$('#ActivityDocument').css('border', '1px solid red');
		success = false;
	}
	
	return success;
}

function validateMessage(){
	var success = true;
	
	$('#MessageDescription').css('border-color', '#d5d5d5');
	
	if ($.trim($('#MessageDescription').val()) == ''){
		$('#MessageDescription').css('border', '1px solid red');
		success = false;
	}
	return success;
}

function validateAddAnalysi(){
	var success = true;
	
	$('#AnalysiFile').css('border-color', '#d5d5d5');
	
	var extension = document.getElementById('AnalysiFile').value;
    extension = extension.split('.').pop();
    extension = extension.toLowerCase();
	if((document.getElementById('AnalysiFile').value == '') || (extension!='xml')){
		$('#AnalysiFile').css('border', '1px solid red');
		success = false;
	}
	
	return success;
}
