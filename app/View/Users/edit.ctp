<?php echo $this -> Html -> script('validate'); ?>
<div class="users form">
<?php echo $this->Form->create('User', array('type' => 'file')); ?>
	<fieldset>
		<legend><h2 style="color:black;"><?php echo 'Atualizar Perfil'; ?></h2></legend>
		<?php
			echo $this->Form->input('id');
		?>
		<?php if($this->Session->read('Auth.User.level') == 2):?>
			<?php
				echo $this->Form->input('course_id', array('disabled'=>'disabled', 'class'=>'form-control', 'style' => 'width:236px;','options'=>$course, 'label'=>'Curso', 'empty'=>array(''=>'Selecione')));
			?>
		<?php else: ?>
			<?php
				echo $this->Form->input('course_id', array('class'=>'form-control', 'style' => 'width:236px;','options'=>$course, 'label'=>'Curso', 'empty'=>array(''=>'Selecione')));
			?>
		<?php endif; ?>
		<?php
			echo $this->Form->input('name', array('class'=>'form-control', 'label'=>'Nome '));
		?>
		<?php
			echo $this->Form->input('level', array('type'=>'hidden'));
			echo $this->Form->input('image_new', array('class'=>'', 'label' => 'Imagem de perfil', 'type'=>'file'));
		?>
		<legend><h4>Informações de Login</h4></legend>
		<?php
			echo $this->Form->input('username', array('class'=>'form-control', 'label' => 'Nome de Usuário '));
			echo $this->Form->input('password_old', array('class'=>'form-control', 'label' => 'Senha Atual ', 'type' => 'password'));
			echo $this->Form->input('password_new', array('class'=>'form-control', 'label' => 'Nova Senha', 'type' => 'password'));
			echo $this->Form->input('password_repeat', array('class'=>'form-control', 'label' => 'Confirmar Senha', 'type' => 'password'));
		?>
	</fieldset>
	<?php echo $this->Form->end(); ?>

	<div class="submit" id="btnSalvar">
		<input type="submit" class="btn btn-info" style="width:75px; height:34px; margin-left:10px;" value="Salvar"/>
	</div>
	</form>
</div>

<div class="users actions">
	<?php if($this->Session->read('Auth.User.level') == 2):?>
		<?php if(isset($analysi['Analysi']['id'])): ?>
	
		<button class="btn btn-info btn-block" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'activities', 'action'=>'show', $this->request->data['Analysi']['id']));?>'">Retornar à Minha Análise</button>
									
		<?php else: ?>
									
			<button class="btn btn-info btn-block" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'analysis', 'action'=>'add'));?>'">Criar Análise</button>
		<?php endif; ?>

	<?php endif ?>
	<?php if($this->Session->read('Auth.User.level') == 3 || $this->Session->read('Auth.User.level') == 4):?>
		<button class="btn btn-info btn-block" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'analysis', 'action'=>'index'));?>'">Retornar à Lista de Análises</button>
	<?php endif ?>
	<?php if($this->Session->read('Auth.User.level') == 5):?>
		<button class="btn btn-info btn-block" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'users', 'action'=>'index'));?>'">Retornar à Painel de Admin</a></li>
	<?php endif; ?>

</div>

<script>

	$('#btnSalvar').click(function(){
		if (validateUserEdit()){
			$('#UserEditForm').submit();
		}
	});

	$('#maskZone').show();
</script>
