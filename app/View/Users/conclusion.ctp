<div class="users form">
<?php echo $this->Form->create('User', array('type' => 'file', 'novalidate' => true)); ?>
	<fieldset>
		<legend><?php echo 'Finalizar cadastro'; ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('course_id', array('class'=>'form-control', 'style' => 'width:236px;', 'options'=>$course, 'label'=>'Curso', 'empty'=>array(''=>'Selecione')));
		echo $this->Form->input('name', array('class'=>'form-control', 'label'=>'Nome'));
		echo $this->Form->input('level', array('type'=>'hidden'));
		echo $this->Form->input('image', array('type'=>'file','label' => array('text' => 'Imagem', 'class' => 'control-label')));
		echo $this->Form->input('username', array('disabled'=>'disabled','class'=>'form-control', 'label' => 'Usuário (email @inf)'));
		echo $this->Form->input('password', array('class'=>'form-control', 'label' => 'Senha'));
	?>
	</fieldset>
<?php echo $this->Form->end(); ?>

<div class="submit" id="btnSalvar">
	<input type="submit" class="btn btn-info" style="width:75px; height:34px; margin-left:10px;" value="Salvar"/>
</div>
</div>


<script>
	$('#btnSalvar').click(function(){
		if (validateUserConclusion()){
			$('#UserConclusionForm').submit();
		}
	});
</script>