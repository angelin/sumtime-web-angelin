<?php echo $this -> Html -> script('validate');  ?>
<div class="analysis form">
<?php echo $this->Form->create('Analysi', array('type' => 'file', 'novalidate'=>true)); ?>
	<fieldset>
		<legend><?php echo ('Criar Análise'); ?></legend>
	<?php
		echo $this->Form->input('user_id', array('type'=>'hidden'));
		echo $this->Form->input('file', array('class'=>'form-control', 'type'=>'file', 'label' => 'Arquivo (.xml)'));
		echo $this->Form->input('status', array('type'=>'hidden'));
	?>
	</fieldset>
	<?php echo $this->Form->end(); ?>

	<div class="submit" id="btnSalvar">
		<input type="submit" class="btn btn-info" style="width:75px; height:34px; margin-left:10px;" value="Salvar"/>
	</div>
</div>

<script>
	$('#btnSalvar').click(function(){
		if (validateAddAnalysi()){
			$('#AnalysiAddForm').submit();
		}
	});
</script>